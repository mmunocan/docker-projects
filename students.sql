CREATE DATABASE studentdb;
use studentdb;

CREATE TABLE student (
  id int unsigned NOT NULL AUTO_INCREMENT,
  name varchar(50) NOT NULL,
  email varchar(100) NOT NULL,
  phone int unsigned NOT NULL,
  address varchar(250) NOT NULL,
  PRIMARY KEY (id)
);

insert  into student(id,name,email,phone,address) values
(1,'John Smith','jsmith@gmail.com',123456789,'Fake St. 123'),
(2,'Loan Almagro','lalmag@gmail.com',987654321,'Av. franklin 1625');

