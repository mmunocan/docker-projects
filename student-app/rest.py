import pymysql
from app import app
from db import mysql
from flask import jsonify
from flask import render_template
from flask import request


@app.route('/', methods=['GET'])
def home():
    return render_template('index.html')

@app.route('/students', methods=['GET'])
def get_students():
    conn = mysql.connect()

    cursor = conn.cursor(pymysql.cursors.DictCursor)
    cursor.execute("SELECT * FROM student")

    rows = cursor.fetchall()

    resp = jsonify(rows)
    resp.status_code = 200

    return resp

@app.route('/student', methods=['GET'])
def get_one_student():
    if 'id' in request.args:
        id = int(request.args['id'])
        conn = mysql.connect()

        cursor = conn.cursor(pymysql.cursors.DictCursor)
        cursor.execute("SELECT * FROM student WHERE id=%s", (id))

        rows = cursor.fetchall()

        resp = jsonify(rows)
        resp.status_code = 200

        return resp

    else:
        return "Error: No id field provided. Please specify an id."

@app.route('/student', methods=['POST'])
def add_student():
    input_json = request.get_json(force=True) 
    name = input_json['name']
    phone = int(input_json['phone'])
    address = input_json['address']
    email = input_json['email']

    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    cursor.execute("INSERT INTO student(name, phone, address, email) VALUES (%s, %s, %s, %s)", (name, phone, address, email))
    conn.commit()

    return '%d rows inserted' % (cursor.rowcount)

@app.route('/student', methods=['DELETE'])
def delete_student():
    if 'id' in request.args:
        id = int(request.args['id'])
        conn = mysql.connect()

        cursor = conn.cursor(pymysql.cursors.DictCursor)
        cursor.execute("DELETE FROM student WHERE id=%s", (id))
        conn.commit()

        return '%d rows deleted' % (cursor.rowcount)

    else:
        return "Error: No id field provided. Please specify an id."


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')


